desc "This task is called by the Heroku scheduler add-on"
task :update_board_and_inputs => :environment do
  Rails.logger.info { "Update board start" }
  @board = Board.last

  if @board.nil?
    Rails.logger.info { "No board initiated, abort" }
    next
  end

  @last_board_ts = @board.unix_timestamp
  if @last_board_ts and Time.now.utc.to_i >= @last_board_ts
    @board.generate_content
  else
    # This should not happend
    Rails.logger.info { "Last record is broken, abort" }
    next
  end
  
  if @board.save
    Rails.logger.info { "Update board done" }
  else
    Rails.logger.info { "Failed to save board" }
    next
  end

  Rails.logger.info { "Clean obsolete inputs start" }
  Input.where('unix_timestamp_millis <= ?', @board.unix_timestamp * 1000).delete_all
  Rails.logger.info { "Clean obsolete inputs done" }
end
