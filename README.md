# Game of Life Backend

## Basic Description

This is the backend part of an online multiplayer version of Game of Life. It allows users to play online in a synchronized view of board.

The frontend is in [another project](https://bitbucket.org/mylesf/game-of-life-frontend/).

This project is hosted at [gol-api.myles.hk](https://gol-api.myles.hk). However, visiting with your browser will get into errors because of the limited routes defined.

## How It Works
For rules of how cells survive to the next round, please refer to [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life). Following are some additional rules and instructions.
* User is assigned a unique ID(integer) with an associated color on start.

* User can click "ASSIGN NEW COLOR" so a new color (together with new user ID) will be assigned.

* User is allowed to enliven cells, all cells enlivened will be given the color of that user.

* Dead cells enlivened by their neighbours(not by the player directly) will be given a color that is the average of its live neighbours.

* Each round of the game is marked by its unix_timestamp, while "updated_at" or "created_at" properties are not considered in computating next round of the board. This means the game is mapped to real world time, so we need a background job to keep computing even no player is online.

* Inputs are marked by its "unix_timestamp_millis" property, which is the milliseconds value of Unix timestamp. Any inputs with the timestamp after the current board timestamp will be applied to the next board as is.

* Computation is done on the server-side only.

## Board Display
The board of cells are encoded and transmitted as Base64 encode binary of PNG file. Each cell is represented by a pixel in the image. The position and color of the pixel matches those of the cell. Alpha channel is not used, and pure white(#FFFFFF) means empty(dead) cell.
However, it is not encoded in Base64 but saved as binary of PNG file in the database.


## Build/Test/Deploy
* Ruby version: 2.5

* System dependencies: A special one is "rmagick". Others are mostly common and specified in Gemfile.

* Some configurations of the application can be found at `/config/application.rb` and `/config/environment.rb`

* Database:
To load the schema, run `$ rails db:schema:load`. No seeding is needed for this project. You may also need to run `$ rails db:reset` when necessary.

* Run test:
`$ rails test`

* Deployment instructions:
`$ rails server -p 8080 -b 0.0.0.0`

This project is ready to be deployed to Heroku, you will need to add the following add-ons:
1. Redis. For ActionCable.

2. Postgres DB. The main database.

3. Heroku Scheduler. To schedule the background task "update_board_and_inputs" located in `/lib/tasks/scheduler.rake`. I recommend you to run the task every 5 to 10 minutes.

For more information, please refer to documents of Heroku.

The Dockerfile included is for development purpose, and you should not deploy it to production.


## Technical Choices
This project is built with Ruby on Rails instead due to its easy setup, greate functionalities and native support to WebSocket. 

WebSocket is chosen for most part of communication with the server, because the client heavily rely on the server (for computation of the next round), and WebSocket is the best choice for its performance due to less overhead involved in this situation.

4 models are created, namely board, cell, input and user, each representing what their name implies.


## Architecture
The system simply relies on Postgres DB as the main database and Redis for supporting ActionCable connections.
Also a background job is required to run every few minutes to keep the board fresh.


## Todos
* Due to time constrains, current tests are not covering all aspects we have.
* Current method are getting list of online users from Redis, while in rare cases, it might be inaccurate. This is to be confirmed.
* Need to find a way to deal with the computation in case the background task does not run for a long time.