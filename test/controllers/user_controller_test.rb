require 'test_helper'

class UserControllerTest < ActionDispatch::IntegrationTest
  test "New user get assigned an ID" do
    post "/api/user"
    assert_response :success
  end

  test "Existing user with correct token" do
    user = User.last

    get "/api/user/#{user.id}?token=#{user.token}"
    assert_response :success
  end

  test "Deny existing user with wrong correct token" do
    user = User.last

    get "/api/user/#{user.id}?token=w#{user.token}"
    assert_response 401
  end

  test "Deny existing user with correct token but wrong ID" do
    user = User.last

    wrong_id = user.id + 1

    get "/api/user/#{wrong_id}?token=#{user.token}"
    assert_response 401
  end
end
