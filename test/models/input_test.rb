require 'test_helper'

class InputTest < ActiveSupport::TestCase
  test "Deny saving new input" do
    input = Input.new
    assert_not input.save
  end

  test "Deny saving input without content" do
    input = Input.new
    input.unix_timestamp_millis = 1540140650293
    input.user_id = 1
    assert_not input.save
  end

  test "Deny saving input without explicit unix_timestamp_millis" do
    input = Input.new
    input.user_id = 1
    input.content = '{"9":[35,36],"10":[37],"11":[34],"12":[35,36],"13":[27,28],"14":[29],"15":[26],"16":[27,28]}'
    assert input.save

    assert input.unix_timestamp_millis
  end

  test "Deny saving input without user_id" do
    input = Input.new
    input.unix_timestamp_millis = 1540140650293
    input.content = '{"9":[35,36],"10":[37],"11":[34],"12":[35,36],"13":[27,28],"14":[29],"15":[26],"16":[27,28]}'
    assert_not input.save
  end

  test "Saving new input" do
    input = Input.new
    input.user_id = 1
    input.unix_timestamp_millis = 1540140650293
    input.content = '{"9":[35,36],"10":[37],"11":[34],"12":[35,36],"13":[27,28],"14":[29],"15":[26],"16":[27,28]}'
    assert input.save
  end

  test "Create input from user and save" do
    user = User.last
    content = '{"9":[35,36],"10":[37],"11":[34],"12":[35,36],"13":[27,28],"14":[29],"15":[26],"16":[27,28]}'
    input = user.inputs.create(content: content)
    input.unix_timestamp_millis = 1540140650293
    assert input.save
  end  


  test "Load input" do
    assert Input.last
  end
end
