require 'test_helper'
require 'rmagick'
include Magick

class BoardTest < ActiveSupport::TestCase
  test "Save new board" do
    board = Board.new
    board.generate_content
    assert board.save
  end

  test "Deny saving new board without content" do
    board = Board.new
    assert_not board.save
  end

  test "Save with updated timestamp" do
    board = Board.last
    new_ts = Time.now.utc.to_i
    id = board.id
    
    board.unix_timestamp = new_ts
    board.save

    board = Board.find(id)
    assert_equal new_ts, board.unix_timestamp
  end

  test "Load board" do
    assert Board.last
  end

  test "Tick to next round" do
    board = Board.last
    last_ts = board.unix_timestamp
    board.generate_content
    assert board.save

    assert_not_equal board.unix_timestamp, last_ts
  end

  test "Json output has additional keys" do
    board = Board.last
    board_json = board.as_json

    assert board_json[:content]
    assert board_json[:interval]
  end
end
