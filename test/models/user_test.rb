require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "Save new user" do
    user = User.new
    assert user.save
  end

  test "Saving user" do
    user = User.new
    user.token = '235029376236'
    assert user.save
  end  

  test "Load user" do
    assert User.last
  end
end
