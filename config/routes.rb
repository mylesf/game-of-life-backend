Rails.application.routes.draw do
    mount ActionCable.server => '/cable'

    scope :api, defaults: {format: :json} do
      resources :user, only: [:create, :show]
    end
end
