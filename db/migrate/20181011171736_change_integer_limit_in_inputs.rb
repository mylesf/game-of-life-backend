class ChangeIntegerLimitInInputs < ActiveRecord::Migration[5.2]
  def change
    change_column :inputs, :unix_timestamp_millis, :integer, limit: 8
  end
end
