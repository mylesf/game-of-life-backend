class CreateBoards < ActiveRecord::Migration[5.2]
  def change
    create_table :boards do |t|
      t.binary :content
      t.integer :unix_timestamp

      t.timestamps
    end
  end
end
