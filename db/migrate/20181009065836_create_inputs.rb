class CreateInputs < ActiveRecord::Migration[5.2]
  def change
    create_table :inputs do |t|
      t.integer :unix_timestamp_millis
      t.text :content
      t.integer :user_id

      t.timestamps
    end
  end
end
