class AddUserRefToInputs < ActiveRecord::Migration[5.2]
  def change
    remove_column :inputs, :user_id, :integer
    add_reference :inputs, :user, foreign_key: true
  end
end
