class RemoveColorFromInputs < ActiveRecord::Migration[5.2]
  def change
    remove_column :inputs, :color, :string
  end
end
