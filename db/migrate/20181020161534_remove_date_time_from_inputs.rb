class RemoveDateTimeFromInputs < ActiveRecord::Migration[5.2]
  def change
    remove_column :inputs, :created_at, :datetime
    remove_column :inputs, :updated_at, :datetime
  end
end
