class RemoveLastOnlineFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :last_online, :integer
  end
end
