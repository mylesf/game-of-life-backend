class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :color
      t.integer :last_online
      t.string :token

      t.timestamps
    end
  end
end
