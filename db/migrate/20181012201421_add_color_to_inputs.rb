class AddColorToInputs < ActiveRecord::Migration[5.2]
  def change
    add_column :inputs, :color, :string
  end
end
