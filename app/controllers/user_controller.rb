class UserController < ApplicationController
  def create
    @user = User.new

    if @user.save
      logger.info { "User \##{@user.id} created" }
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def show
    begin
      @user = User.find(params[:id])
    rescue
      return decline_unauthorized_attemp
    end

    if @user.token === params[:token]
      @user.regenerate_token

      logger.info { "User \##{@user.id} logged in" }

      if @user.save
        render json: @user, location: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    else
      return decline_unauthorized_attemp
    end
  end

  private
  def decline_unauthorized_attemp
    render status: :unauthorized 
  end
end
