class Cell
  extend ActiveModel::Naming
  
  attr_reader :board, :x, :y, :red, :green, :blue, :max_color
  attr_accessor :new_red, :new_green, :new_blue

  def initialize(board, x, y, red=nil, green=nil, blue=nil, max_color=255)
    @board, @x, @y, @max_color = board, x, y, max_color
    if red and green and blue
      apply_color(red, green, blue)
    else
      dead!
    end
  end

  def dead?
    @red == @max_color and @green == @max_color and @blue == @max_color
  end

  def dead!
    apply_color(@max_color, @max_color, @max_color)
  end

  def live?
    !dead?
  end

  def apply_color(red=nil, green=nil, blue=nil)
    if red and green and blue
      @red, @green, @blue = Integer(red), Integer(green), Integer(blue)
    end
  end

  def live!(red=nil, green=nil, blue=nil)
    if red and green and blue
      apply_color(red, green, blue)
    elsif @new_red and @new_green and @new_blue
      apply_color(@new_red, @new_green, @new_blue)

      @new_red, @new_green, @new_blue = [nil] * 3
    end
  end

  def neighbours
    neighbours = []

    (-1..1).each do |delta_x|
      (-1..1).each do |delta_y|
        neighbour_candidate = @board.get_cell(@x + delta_x, @y + delta_y)
        if (delta_x == 0 and delta_y == 0) or !neighbour_candidate or !neighbour_candidate.live?
          next
        end

        neighbours.push(neighbour_candidate)
      end
    end
    neighbours
  end
end