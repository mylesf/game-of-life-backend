class User < ApplicationRecord
  has_secure_token
  has_many :inputs

  before_create :set_color
  
  private

    def set_color
      self.color = unique_random_color
    end

    def unique_random_color
      loop do
        color = SecureRandom.hex(3)
        break color unless User.where(color: color).exists?
      end
    end
end
