class Input < ApplicationRecord
  belongs_to :user
  validates :content, :user_id, presence: true

  before_create do
    self.unix_timestamp_millis = (Time.now.utc.to_f * 1000).ceil
  end

  def color
    begin
      @user = User.find(self.user_id)
    rescue
      return nil
    end
    @user[:color]
  end

  def as_json(*)
    super.tap do |hash|
      hash[:color] = color
    end
  end

end
