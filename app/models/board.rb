require 'rmagick'
include Magick
require 'json'

class Board < ApplicationRecord
  validates :content, presence: true

  WIDTH = Rails.configuration.board_width
  HEIGHT = Rails.configuration.board_height
  INTERVAL = Rails.configuration.board_step_interval

  before_create do
    self.unix_timestamp = Time.now.utc
  end

  def load_cells_from_content
    @canvas = Image.from_blob(self.content){self.depth = 8}.first

    @cells = []

    WIDTH.times do |x|
      @cells[x] = []
      HEIGHT.times do |y|
        pixel = @canvas.pixel_color(x, y)
        @cells[x][y] = Cell.new(self, x, y, pixel.red, pixel.green, pixel.blue, QuantumRange)
      end
    end
  end

  def cells
    if @cells
      @cells.flatten
    else
      []
    end
  end

  def write_cells_to_content
    cells.each do |cell|
      @canvas.pixel_color(cell.x, cell.y, Pixel.new(cell.red, cell.green, cell.blue))
    end

    self.content = @canvas.to_blob
  end

  def get_cell(x, y)
    if @cells[x]
      @cells[x][y]
    end
  end
  
  def as_json(*)
    super.except("created_at", "updated_at").tap do |output_hash|
      output_hash[:content] = b64_content
      output_hash[:interval] = Rails.configuration.board_step_interval
    end
  end

  def generate_content(go_next=true)
    current_ts = Time.now.utc.to_i

    if self.content
      load_cells_from_content
      
      apply_inputs_to_cells

      while self.unix_timestamp + INTERVAL <= current_ts and go_next do
        next_generation!
        self.unix_timestamp = INTERVAL + self.unix_timestamp
        # apply inputs to the new board without stepping
        apply_inputs_to_cells
        logger.debug { "New board #{self.unix_timestamp} generated at #{Time.now.utc.to_i}" }
      end

      write_cells_to_content
    else
      generate_empty_board_content
      self.unix_timestamp = current_ts
      # b/c board is just initialized, ignore earlier inputs, and do not step forward
    end
  end

  def unapplied_inputs
    Input.where('unix_timestamp_millis > ?', self.unix_timestamp * 1000).order(:unix_timestamp_millis)
  end

  private
  def next_generation!
    to_die_buffer = []
    to_live_buffer = []

    cells.each do |cell|
      neighbours = cell.neighbours
      num_of_neighbours = neighbours.length
      
      if num_of_neighbours < 2 or num_of_neighbours > 3
        if cell.live?
          to_die_buffer.push(cell)
        end
      elsif num_of_neighbours == 3
        if cell.dead?
          # calculate new color
          sum_red, sum_green, sum_blue = [0] * 3

          neighbours.each do |neighbour|
            sum_red += neighbour.red
            sum_green += neighbour.green
            sum_blue += neighbour.blue
          end
          cell.new_red, cell.new_green, cell.new_blue = sum_red / 3, sum_green / 3, sum_blue / 3
          to_live_buffer.push(cell)
        end
      end
    end

    # if you live, you need to privide your color
    to_live_buffer.each(&:live!)

    # if you die, you can go straight away
    to_die_buffer.each(&:dead!)
    return
  end
  
  def generate_empty_board_content
    # generate empty canvas
    @canvas = Image.new(WIDTH, HEIGHT) do |c|
      c.format = 'PNG'
      c.depth = 8
      c.background_color= "white"
    end

    self.content = @canvas.to_blob
  end

  def apply_inputs_to_cells
    Input.where(unix_timestamp_millis: (self.unix_timestamp * 1000)..((self.unix_timestamp + INTERVAL) * 1000 - 1)).order(:unix_timestamp_millis).each do |input|
      color = input.color
      if color.blank?
        # skip if no color is found
        next
      end
      input_hash = JSON.parse(input[:content])
      input_hash.each do |x, array|
        x = Integer(x)
        array.each do |y|
          pixel = Pixel.from_color("#" + color)
          cell = get_cell(x, y).live!(pixel.red, pixel.green, pixel.blue)
        end
      end
    end
  end

  def b64_content
    Base64.encode64(self.content)
  end
end
