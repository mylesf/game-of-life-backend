module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
    end

    def disconnect
      self.class.send_users
    end

    def self.send_users
      @users = []

      pubsub = ActionCable.server.pubsub
      channels = pubsub.send(:redis_connection).pubsub('channels', "action_cable/*")
      subscriptions = channels.map do |channel|
        full_identifier= Base64.decode64(channel.match(/^action_cable\/(.*)$/)[1])
        user_id = full_identifier.match(/\/User\/(\d+)$/)[1]
        user = User.select(:id, :color).find_by(id: user_id)

        if user
          @users.push(user)
        end
      end

      ActionCable.server.broadcast "main", {event: "online_users_change", data: { users: @users }}
    end

    private
    def find_verified_user
      user = User.select(:id, :color).find_by(token: request.params[:token], id: request.params[:id])
      
      if not user
        reject_unauthorized_connection
      end
      user
    end
  end
end
