require 'json'
class MainChannel < ApplicationCable::Channel
  periodically every: 1.seconds do
    if @last_sent_ts and Time.now.utc.to_i - @last_sent_ts < 1
      next
    end
    @board = @board || Board.last || Board.new
    @last_board_ts = @last_board_ts || @board.unix_timestamp
    if @last_board_ts and Time.now.utc.to_i >= @last_board_ts + Rails.configuration.board_step_interval
      
      Rails.logger.debug { "Real send_board start #{Time.now.utc.to_i}, last_sent #{@last_sent_ts}, last_broad #{@last_board_ts}" }
      send_board
      @last_sent_ts = Time.now.utc.to_i

      Rails.logger.debug { "Finished at #{Time.now.utc.to_i}" }
    end
  end

  def subscribed
    stream_from "main_#{current_user.id}"
    stream_from "main"
    send_board

    ApplicationCable::Connection.send_users
  end

  def receive(data)
    case data["event"]
    when "input"
      on_input data["data"]["content"].to_json
    when "next_step"
      if @last_board_ts and Time.now.utc.to_i >= @last_board_ts + 5
        send_board
      else
        ActionCable.server.broadcast "main_#{current_user.id}", {event: "board_no_change", data: {ts: Time.now.utc, last_ts: @last_board_ts}}
      end

    when "time"
      client_time = data["data"]["client_time"].to_i
      server_time = Time.now.utc.to_f.round(3) * 1000
      ActionCable.server.broadcast "main_#{current_user.id}", {event: "time", data: {server_time: server_time, client_time: client_time}}
    else
      # Slip unknown event
    end
  end

  def unsubscribed
    # Here the user record still exists. We check on connection disconnect instead
  end

  def send_board(go_next=true)
    @board = Board.last || Board.new
    @board.generate_content(go_next)
    @last_board_ts = @board.unix_timestamp
    
    ActionCable.server.broadcast "main_#{current_user.id}", {event: "board_change", data: {board: @board.as_json}}
    
    unapplied_inputs = @board.unapplied_inputs
    if @last_unapplied_inputs.nil? or unapplied_inputs.sort != @last_unapplied_inputs.sort
      ActionCable.server.broadcast "main_#{current_user.id}", {data: { inputs: unapplied_inputs}, event: "inputs_change" }
      @last_unapplied_inputs = unapplied_inputs
    end
    
    @board.save
  end

  private

  def on_input(content)
    @input = current_user.inputs.create(content: content)

    if @input.save
      @board = @board || Board.last || Board.new

      ActionCable.server.broadcast "main", {data: { inputs: @board.unapplied_inputs}, event: "inputs_change" }
    else
      ActionCable.server.broadcast "main_#{current_user.id}", {data: @input, event: "new_input_rejected" }
    end
  end
end
