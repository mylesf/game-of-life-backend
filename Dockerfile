FROM ruby:2.5

ARG UID
RUN adduser rails --uid $UID --disabled-password --gecos ""

ENV APP /usr/src/app
RUN mkdir $APP
WORKDIR $APP

COPY Gemfile* $APP/
COPY Gemfile.lock $APP/
RUN bundle install --path vendor/bundle
RUN gem install rails

COPY . $APP/

CMD ["rails", "server", "-p", "8080", "-b", "0.0.0.0", "--pid", "/tmp/rails_server.pid"]
